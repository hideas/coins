﻿namespace Sosnovskiy.Homework10
{
    partial class Coins
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Coins));
            this.pbCoin_1 = new System.Windows.Forms.PictureBox();
            this.pbCoin_2 = new System.Windows.Forms.PictureBox();
            this.pbCoin_3 = new System.Windows.Forms.PictureBox();
            this.pbCoin_Invisible = new System.Windows.Forms.PictureBox();
            this.pbCoin_4 = new System.Windows.Forms.PictureBox();
            this.pbCoin_5 = new System.Windows.Forms.PictureBox();
            this.pbCoin_6 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbCoin_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCoin_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCoin_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCoin_Invisible)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCoin_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCoin_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCoin_6)).BeginInit();
            this.SuspendLayout();
            // 
            // pbCoin_1
            // 
            this.pbCoin_1.BackColor = System.Drawing.Color.Snow;
            this.pbCoin_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbCoin_1.Image = ((System.Drawing.Image)(resources.GetObject("pbCoin_1.Image")));
            this.pbCoin_1.Location = new System.Drawing.Point(0, 0);
            this.pbCoin_1.Name = "pbCoin_1";
            this.pbCoin_1.Size = new System.Drawing.Size(60, 60);
            this.pbCoin_1.TabIndex = 0;
            this.pbCoin_1.TabStop = false;
            // 
            // pbCoin_2
            // 
            this.pbCoin_2.BackColor = System.Drawing.Color.Snow;
            this.pbCoin_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbCoin_2.Image = ((System.Drawing.Image)(resources.GetObject("pbCoin_2.Image")));
            this.pbCoin_2.Location = new System.Drawing.Point(60, 0);
            this.pbCoin_2.Name = "pbCoin_2";
            this.pbCoin_2.Size = new System.Drawing.Size(60, 60);
            this.pbCoin_2.TabIndex = 1;
            this.pbCoin_2.TabStop = false;
            // 
            // pbCoin_3
            // 
            this.pbCoin_3.BackColor = System.Drawing.Color.Snow;
            this.pbCoin_3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbCoin_3.Image = ((System.Drawing.Image)(resources.GetObject("pbCoin_3.Image")));
            this.pbCoin_3.Location = new System.Drawing.Point(120, 0);
            this.pbCoin_3.Name = "pbCoin_3";
            this.pbCoin_3.Size = new System.Drawing.Size(60, 60);
            this.pbCoin_3.TabIndex = 2;
            this.pbCoin_3.TabStop = false;
            // 
            // pbCoin_Invisible
            // 
            this.pbCoin_Invisible.BackColor = System.Drawing.Color.Snow;
            this.pbCoin_Invisible.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbCoin_Invisible.Location = new System.Drawing.Point(180, 0);
            this.pbCoin_Invisible.Name = "pbCoin_Invisible";
            this.pbCoin_Invisible.Size = new System.Drawing.Size(60, 60);
            this.pbCoin_Invisible.TabIndex = 3;
            this.pbCoin_Invisible.TabStop = false;
            this.pbCoin_Invisible.Visible = false;
            // 
            // pbCoin_4
            // 
            this.pbCoin_4.BackColor = System.Drawing.Color.Snow;
            this.pbCoin_4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbCoin_4.Image = ((System.Drawing.Image)(resources.GetObject("pbCoin_4.Image")));
            this.pbCoin_4.Location = new System.Drawing.Point(240, 0);
            this.pbCoin_4.Name = "pbCoin_4";
            this.pbCoin_4.Size = new System.Drawing.Size(60, 60);
            this.pbCoin_4.TabIndex = 4;
            this.pbCoin_4.TabStop = false;
            // 
            // pbCoin_5
            // 
            this.pbCoin_5.BackColor = System.Drawing.Color.Snow;
            this.pbCoin_5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbCoin_5.Image = ((System.Drawing.Image)(resources.GetObject("pbCoin_5.Image")));
            this.pbCoin_5.Location = new System.Drawing.Point(300, 0);
            this.pbCoin_5.Name = "pbCoin_5";
            this.pbCoin_5.Size = new System.Drawing.Size(60, 60);
            this.pbCoin_5.TabIndex = 5;
            this.pbCoin_5.TabStop = false;
            // 
            // pbCoin_6
            // 
            this.pbCoin_6.BackColor = System.Drawing.Color.Snow;
            this.pbCoin_6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbCoin_6.Image = ((System.Drawing.Image)(resources.GetObject("pbCoin_6.Image")));
            this.pbCoin_6.Location = new System.Drawing.Point(360, 0);
            this.pbCoin_6.Name = "pbCoin_6";
            this.pbCoin_6.Size = new System.Drawing.Size(60, 60);
            this.pbCoin_6.TabIndex = 6;
            this.pbCoin_6.TabStop = false;
            // 
            // Coins
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Snow;
            this.ClientSize = new System.Drawing.Size(420, 61);
            this.Controls.Add(this.pbCoin_6);
            this.Controls.Add(this.pbCoin_5);
            this.Controls.Add(this.pbCoin_4);
            this.Controls.Add(this.pbCoin_Invisible);
            this.Controls.Add(this.pbCoin_3);
            this.Controls.Add(this.pbCoin_2);
            this.Controls.Add(this.pbCoin_1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Coins";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Coins";
            this.Load += new System.EventHandler(this.ArrowGame_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbCoin_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCoin_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCoin_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCoin_Invisible)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCoin_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCoin_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCoin_6)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pbCoin_1;
        private System.Windows.Forms.PictureBox pbCoin_2;
        private System.Windows.Forms.PictureBox pbCoin_3;
        private System.Windows.Forms.PictureBox pbCoin_Invisible;
        private System.Windows.Forms.PictureBox pbCoin_4;
        private System.Windows.Forms.PictureBox pbCoin_5;
        private System.Windows.Forms.PictureBox pbCoin_6;
    }
}

